/*
 * This file was automatically generated by EvoSuite
 */

package com.eteks.sweethome3d.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import com.eteks.sweethome3d.model.HomeTexture;
import com.eteks.sweethome3d.model.Level;
import com.eteks.sweethome3d.model.Room;
import com.eteks.sweethome3d.model.TextStyle;
import java.beans.PropertyChangeListener;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class RoomEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      float[][] floatArray0 = new float[17][6];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      boolean boolean0 = room0.isNameCenterPointAt((-1.0F), 0.0F, 1690.2755F);
      assertEquals(true, room0.isFloorVisible());
      assertEquals(true, boolean0);
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isAreaVisible());
      assertEquals(0.0F, room0.getXCenter(), 0.01F);
      assertEquals(0.0F, room0.getNameXOffset(), 0.01F);
      assertEquals(0.0F, room0.getYCenter(), 0.01F);
      assertEquals(true, room0.isCeilingVisible());
  }

  @Test
  public void test1()  throws Throwable  {
      float[][] floatArray0 = new float[17][6];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.getName();
      assertEquals(true, room0.isCeilingVisible());
      assertEquals(true, room0.isAreaVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isFloorVisible());
  }

  @Test
  public void test2()  throws Throwable  {
      float[][] floatArray0 = new float[10][6];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      boolean boolean0 = room0.isAreaCenterPointAt(0.0F, (-1450.6653F), 1773.636F);
      assertEquals(0.0F, room0.getYCenter(), 0.01F);
      assertEquals(0.0F, room0.getAreaXOffset(), 0.01F);
      assertEquals(0.0F, room0.getXCenter(), 0.01F);
      assertEquals(true, room0.isFloorVisible());
      assertEquals(true, boolean0);
      assertEquals(true, room0.isAreaVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
  }

  @Test
  public void test3()  throws Throwable  {
      float[][] floatArray0 = new float[10][4];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.containsPoint(0.0F, 0.0F, 0.0F);
      assertEquals(true, room0.isFloorVisible());
      assertEquals(true, room0.isAreaVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isCeilingVisible());
  }

  @Test
  public void test4()  throws Throwable  {
      float[][] floatArray0 = new float[2][7];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.getLevel();
      assertEquals(true, room0.isCeilingVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isAreaVisible());
      assertEquals(true, room0.isFloorVisible());
  }

  @Test
  public void test5()  throws Throwable  {
      float[][] floatArray0 = new float[10][4];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      float float0 = room0.getCeilingShininess();
      assertEquals(true, room0.isAreaVisible());
      assertEquals(true, room0.isFloorVisible());
      assertEquals(true, room0.isCeilingVisible());
      assertEquals(0.0F, float0, 0.01F);
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
  }

  @Test
  public void test6()  throws Throwable  {
      float[][] floatArray0 = new float[2][2];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.getCeilingTexture();
      assertEquals(true, room0.isAreaVisible());
      assertEquals(true, room0.isCeilingVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isFloorVisible());
  }

  @Test
  public void test7()  throws Throwable  {
      float[][] floatArray0 = new float[10][4];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.getAreaStyle();
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isCeilingVisible());
      assertEquals(true, room0.isFloorVisible());
      assertEquals(true, room0.isAreaVisible());
  }

  @Test
  public void test8()  throws Throwable  {
      float[][] floatArray0 = new float[10][3];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      Room room1 = room0.clone();
      assertEquals(true, room0.isCeilingVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isAreaVisible());
      assertEquals(true, room0.isFloorVisible());
      assertNotSame(room0, room1);
  }

  @Test
  public void test9()  throws Throwable  {
      float[][] floatArray0 = new float[17][6];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.getPointCount();
      assertEquals(true, room0.isAreaVisible());
      assertEquals(true, room0.isCeilingVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isFloorVisible());
  }

  @Test
  public void test10()  throws Throwable  {
      float[][] floatArray0 = new float[10][3];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.getFloorColor();
      assertEquals(true, room0.isAreaVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isCeilingVisible());
      assertEquals(true, room0.isFloorVisible());
  }

  @Test
  public void test11()  throws Throwable  {
      float[][] floatArray0 = new float[8][8];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.removePropertyChangeListener((PropertyChangeListener) null);
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isFloorVisible());
      assertEquals(true, room0.isCeilingVisible());
      assertEquals(true, room0.isAreaVisible());
  }

  @Test
  public void test12()  throws Throwable  {
      float[][] floatArray0 = new float[17][6];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      boolean boolean0 = room0.isSingular();
      assertEquals(true, boolean0);
      assertEquals(true, room0.isFloorVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isCeilingVisible());
      assertEquals(true, room0.isAreaVisible());
  }

  @Test
  public void test13()  throws Throwable  {
      float[][] floatArray0 = new float[10][6];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      boolean boolean0 = room0.isAreaVisible();
      assertEquals(true, room0.isFloorVisible());
      assertEquals(true, boolean0);
      assertEquals(true, room0.isCeilingVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
  }

  @Test
  public void test14()  throws Throwable  {
      float[][] floatArray0 = new float[7][3];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.getFloorTexture();
      assertEquals(true, room0.isAreaVisible());
      assertEquals(true, room0.isCeilingVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isFloorVisible());
  }

  @Test
  public void test15()  throws Throwable  {
      float[][] floatArray0 = new float[10][4];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.getNameStyle();
      assertEquals(true, room0.isCeilingVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isFloorVisible());
      assertEquals(true, room0.isAreaVisible());
  }

  @Test
  public void test16()  throws Throwable  {
      float[][] floatArray0 = new float[8][2];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.getCeilingColor();
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isFloorVisible());
      assertEquals(true, room0.isAreaVisible());
      assertEquals(true, room0.isCeilingVisible());
  }

  @Test
  public void test17()  throws Throwable  {
      float[][] floatArray0 = new float[10][4];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.move(0.0F, (-40.0F));
      assertEquals((-40.0F), room0.getYCenter(), 0.01F);
  }

  @Test
  public void test18()  throws Throwable  {
      float[][] floatArray0 = new float[2][2];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.addPropertyChangeListener((PropertyChangeListener) null);
      assertEquals(true, room0.isAreaVisible());
      assertEquals(true, room0.isCeilingVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isFloorVisible());
  }

  @Test
  public void test19()  throws Throwable  {
      float[][] floatArray0 = new float[2][7];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      float float0 = room0.getAreaAngle();
      assertEquals(true, room0.isCeilingVisible());
      assertEquals(true, room0.isFloorVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(0.0F, float0, 0.01F);
      assertEquals(true, room0.isAreaVisible());
  }

  @Test
  public void test20()  throws Throwable  {
      float[][] floatArray0 = new float[10][4];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.intersectsRectangle((-1.0F), 0.0F, 0.0F, 0.0F);
      assertEquals(true, room0.isAreaVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isCeilingVisible());
      assertEquals(true, room0.isFloorVisible());
  }

  @Test
  public void test21()  throws Throwable  {
      float[][] floatArray0 = new float[10][6];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      float float0 = room0.getNameAngle();
      assertEquals(true, room0.isCeilingVisible());
      assertEquals(true, room0.isFloorVisible());
      assertEquals(true, room0.isAreaVisible());
      assertEquals(0.0F, float0, 0.01F);
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
  }

  @Test
  public void test22()  throws Throwable  {
      float[][] floatArray0 = new float[10][3];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      boolean boolean0 = room0.isCeilingVisible();
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, boolean0);
      assertEquals(true, room0.isFloorVisible());
      assertEquals(true, room0.isAreaVisible());
  }

  @Test
  public void test23()  throws Throwable  {
      float[][] floatArray0 = new float[2][2];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      boolean boolean0 = room0.isFloorVisible();
      assertEquals(true, room0.isAreaVisible());
      assertEquals(true, room0.isCeilingVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, boolean0);
  }

  @Test
  public void test24()  throws Throwable  {
      float[][] floatArray0 = new float[7][3];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.addPoint(622.1518F, 1.0F);
      assertEquals(8, room0.getPointCount());
      assertEquals(311.0759F, room0.getXCenter(), 0.01F);
  }

  @Test
  public void test25()  throws Throwable  {
      float[][] floatArray0 = new float[2][2];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      float float0 = room0.getFloorShininess();
      assertEquals(true, room0.isCeilingVisible());
      assertEquals(true, room0.isFloorVisible());
      assertEquals(0.0F, float0, 0.01F);
      assertEquals(true, room0.isAreaVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
  }

  @Test
  public void test26()  throws Throwable  {
      float[][] floatArray0 = new float[1][8];
      Room room0 = null;
      try {
        room0 = new Room(floatArray0);
        fail("Expecting exception: IllegalStateException");
      } catch(IllegalStateException e) {
        /*
         * Room points must containt at least two points
         */
      }
  }

  @Test
  public void test27()  throws Throwable  {
      float[][] floatArray0 = new float[2][2];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.setName("DAs~ZBDQw-whEj");
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isFloorVisible());
      assertEquals(true, room0.isAreaVisible());
      assertEquals(true, room0.isCeilingVisible());
  }

  @Test
  public void test28()  throws Throwable  {
      float[][] floatArray0 = new float[6][10];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.setName((String) null);
      assertEquals(true, room0.isCeilingVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isAreaVisible());
      assertEquals(true, room0.isFloorVisible());
  }

  @Test
  public void test29()  throws Throwable  {
      float[][] floatArray0 = new float[10][3];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.setNameXOffset(0.0F);
      assertEquals(true, room0.isAreaVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isFloorVisible());
      assertEquals(true, room0.isCeilingVisible());
  }

  @Test
  public void test30()  throws Throwable  {
      float[][] floatArray0 = new float[10][6];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.setNameXOffset(1.0F);
      assertEquals(1.0F, room0.getNameXOffset(), 0.01F);
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
  }

  @Test
  public void test31()  throws Throwable  {
      float[][] floatArray0 = new float[7][9];
      Room room0 = new Room(floatArray0);
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertNotNull(room0);
      
      room0.setNameYOffset(0.0F);
      room0.setNameYOffset(0.0F);
      assertEquals(0.0F, room0.getNameYOffset(), 0.01F);
  }

  @Test
  public void test32()  throws Throwable  {
      float[][] floatArray0 = new float[10][4];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.setNameStyle((TextStyle) null);
      assertEquals(true, room0.isCeilingVisible());
      assertEquals(true, room0.isAreaVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isFloorVisible());
  }

  @Test
  public void test33()  throws Throwable  {
      float[][] floatArray0 = new float[9][6];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      TextStyle textStyle0 = new TextStyle(1812.6902F, true, false);
      room0.setNameStyle(textStyle0);
      assertEquals(true, room0.isAreaVisible());
      assertEquals(true, room0.isCeilingVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isFloorVisible());
  }

  @Test
  public void test34()  throws Throwable  {
      float[][] floatArray0 = new float[2][7];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.setNameAngle(0.0F);
      assertEquals(true, room0.isCeilingVisible());
      assertEquals(true, room0.isAreaVisible());
      assertEquals(true, room0.isFloorVisible());
      assertEquals(0.0F, room0.getNameAngle(), 0.01F);
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
  }

  @Test
  public void test35()  throws Throwable  {
      float[][] floatArray0 = new float[17][6];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.setNameAngle(3.4799218F);
      assertEquals(3.4799218F, room0.getNameAngle(), 0.01F);
  }

  @Test
  public void test36()  throws Throwable  {
      float[][] floatArray0 = new float[2][2];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.setPoints(floatArray0);
      assertEquals(true, room0.isAreaVisible());
      assertEquals(true, room0.isFloorVisible());
      assertEquals(true, room0.isCeilingVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
  }

  @Test
  public void test37()  throws Throwable  {
      float[][] floatArray0 = new float[2][2];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      float[] floatArray1 = new float[7];
      floatArray0[1] = floatArray1;
      room0.setPoints(floatArray0);
      assertEquals(true, room0.isCeilingVisible());
      assertEquals(true, room0.isFloorVisible());
      assertEquals(true, room0.isAreaVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
  }

  @Test
  public void test38()  throws Throwable  {
      float[][] floatArray0 = new float[2][3];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      // Undeclared exception!
      try {
        room0.addPoint((-1.0F), 1637.0865F, 7);
        fail("Expecting exception: IndexOutOfBoundsException");
      } catch(IndexOutOfBoundsException e) {
        /*
         * Invalid index 7
         */
      }
  }

  @Test
  public void test39()  throws Throwable  {
      float[][] floatArray0 = new float[10][6];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.setPoint((-1413.4648F), (-152.9524F), 1);
      assertEquals((-76.4762F), room0.getYCenter(), 0.01F);
      assertEquals(0.0F, room0.getArea(), 0.01F);
  }

  @Test
  public void test40()  throws Throwable  {
      float[][] floatArray0 = new float[3][3];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      Integer integer0 = new Integer(1450);
      // Undeclared exception!
      try {
        room0.setPoint(0.0F, 0.0F, (int) integer0);
        fail("Expecting exception: IndexOutOfBoundsException");
      } catch(IndexOutOfBoundsException e) {
        /*
         * Invalid index 1450
         */
      }
  }

  @Test
  public void test41()  throws Throwable  {
      float[][] floatArray0 = new float[3][3];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.setPoint(0.0F, 0.0F, 0);
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isFloorVisible());
      assertEquals(true, room0.isCeilingVisible());
      assertEquals(true, room0.isAreaVisible());
  }

  @Test
  public void test42()  throws Throwable  {
      float[][] floatArray0 = new float[3][3];
      float[] floatArray1 = new float[7];
      floatArray1[1] = 638.04663F;
      floatArray0[0] = floatArray1;
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.setPoint(0.0F, 0.0F, 0);
      assertEquals(0.0F, room0.getYCenter(), 0.01F);
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
  }

  @Test
  public void test43()  throws Throwable  {
      float[][] floatArray0 = new float[7][9];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      // Undeclared exception!
      try {
        room0.removePoint((-838));
        fail("Expecting exception: IndexOutOfBoundsException");
      } catch(IndexOutOfBoundsException e) {
        /*
         * Invalid index -838
         */
      }
  }

  @Test
  public void test44()  throws Throwable  {
      float[][] floatArray0 = new float[10][4];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      // Undeclared exception!
      try {
        room0.removePoint(1518);
        fail("Expecting exception: IndexOutOfBoundsException");
      } catch(IndexOutOfBoundsException e) {
        /*
         * Invalid index 1518
         */
      }
  }

  @Test
  public void test45()  throws Throwable  {
      float[][] floatArray0 = new float[10][4];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.removePoint(0);
      assertEquals(9, room0.getPointCount());
  }

  @Test
  public void test46()  throws Throwable  {
      float[][] floatArray0 = new float[9][6];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      assertEquals(true, room0.isAreaVisible());
      
      room0.setAreaVisible(true);
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isCeilingVisible());
      assertEquals(true, room0.isFloorVisible());
  }

  @Test
  public void test47()  throws Throwable  {
      float[][] floatArray0 = new float[10][6];
      Room room0 = new Room(floatArray0);
      assertEquals(true, room0.isAreaVisible());
      assertNotNull(room0);
      
      room0.setAreaVisible(false);
      assertEquals(false, room0.isAreaVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
  }

  @Test
  public void test48()  throws Throwable  {
      float[][] floatArray0 = new float[10][3];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.setAreaXOffset(0.0F);
      assertEquals(true, room0.isFloorVisible());
      assertEquals(true, room0.isCeilingVisible());
      assertEquals(true, room0.isAreaVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
  }

  @Test
  public void test49()  throws Throwable  {
      float[][] floatArray0 = new float[2][2];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.setAreaXOffset(985.4346F);
      assertEquals(985.4346F, room0.getAreaXOffset(), 0.01F);
  }

  @Test
  public void test50()  throws Throwable  {
      float[][] floatArray0 = new float[10][4];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.setAreaYOffset(0.0F);
      assertEquals(true, room0.isCeilingVisible());
      assertEquals(true, room0.isAreaVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isFloorVisible());
  }

  @Test
  public void test51()  throws Throwable  {
      float[][] floatArray0 = new float[10][3];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.setAreaYOffset(1.0F);
      assertEquals(1.0F, room0.getAreaYOffset(), 0.01F);
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
  }

  @Test
  public void test52()  throws Throwable  {
      float[][] floatArray0 = new float[10][4];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.setAreaStyle((TextStyle) null);
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isAreaVisible());
      assertEquals(true, room0.isCeilingVisible());
      assertEquals(true, room0.isFloorVisible());
  }

  @Test
  public void test53()  throws Throwable  {
      float[][] floatArray0 = new float[7][7];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      TextStyle textStyle0 = new TextStyle((-101.71726F), false, false);
      room0.setAreaStyle(textStyle0);
      assertEquals(true, room0.isCeilingVisible());
      assertEquals(true, room0.isFloorVisible());
      assertEquals(true, room0.isAreaVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
  }

  @Test
  public void test54()  throws Throwable  {
      float[][] floatArray0 = new float[10][6];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.setAreaAngle(0.0F);
      assertEquals(true, room0.isFloorVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isCeilingVisible());
      assertEquals(0.0F, room0.getAreaAngle(), 0.01F);
      assertEquals(true, room0.isAreaVisible());
  }

  @Test
  public void test55()  throws Throwable  {
      float[][] floatArray0 = new float[10][3];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.setAreaAngle(1662.4082F);
      assertEquals(3.6472821F, room0.getAreaAngle(), 0.01F);
  }

  @Test
  public void test56()  throws Throwable  {
      float[][] floatArray0 = new float[2][7];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      Integer integer0 = new Integer(814);
      room0.setFloorColor(integer0);
      assertEquals(true, room0.isFloorVisible());
      assertEquals(true, room0.isCeilingVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isAreaVisible());
  }

  @Test
  public void test57()  throws Throwable  {
      float[][] floatArray0 = new float[8][2];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.setFloorColor((Integer) null);
      assertEquals(true, room0.isAreaVisible());
      assertEquals(true, room0.isCeilingVisible());
      assertEquals(true, room0.isFloorVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
  }

  @Test
  public void test58()  throws Throwable  {
      float[][] floatArray0 = new float[3][3];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.setFloorTexture((HomeTexture) null);
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isCeilingVisible());
      assertEquals(true, room0.isFloorVisible());
      assertEquals(true, room0.isAreaVisible());
  }

  @Test
  public void test59()  throws Throwable  {
      float[][] floatArray0 = new float[10][3];
      Room room0 = new Room(floatArray0);
      assertEquals(true, room0.isFloorVisible());
      assertNotNull(room0);
      
      room0.setFloorVisible(true);
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isAreaVisible());
      assertEquals(true, room0.isCeilingVisible());
  }

  @Test
  public void test60()  throws Throwable  {
      float[][] floatArray0 = new float[7][9];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.setFloorVisible(false);
      assertEquals(false, room0.isFloorVisible());
      
      room0.setFloorVisible(true);
      assertEquals(true, room0.isFloorVisible());
  }

  @Test
  public void test61()  throws Throwable  {
      float[][] floatArray0 = new float[2][2];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.setFloorShininess(0.0F);
      assertEquals(true, room0.isFloorVisible());
      assertEquals(true, room0.isAreaVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isCeilingVisible());
  }

  @Test
  public void test62()  throws Throwable  {
      float[][] floatArray0 = new float[3][3];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.setFloorShininess((-1.0F));
      assertEquals((-1.0F), room0.getFloorShininess(), 0.01F);
  }

  @Test
  public void test63()  throws Throwable  {
      float[][] floatArray0 = new float[3][3];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      Integer integer0 = new Integer(1450);
      room0.setCeilingColor(integer0);
      assertEquals(true, room0.isAreaVisible());
      assertEquals(true, room0.isCeilingVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isFloorVisible());
  }

  @Test
  public void test64()  throws Throwable  {
      float[][] floatArray0 = new float[9][6];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.setCeilingColor((Integer) null);
      assertEquals(true, room0.isFloorVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isCeilingVisible());
      assertEquals(true, room0.isAreaVisible());
  }

  @Test
  public void test65()  throws Throwable  {
      float[][] floatArray0 = new float[2][2];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.setCeilingTexture((HomeTexture) null);
      assertEquals(true, room0.isAreaVisible());
      assertEquals(true, room0.isFloorVisible());
      assertEquals(true, room0.isCeilingVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
  }

  @Test
  public void test66()  throws Throwable  {
      float[][] floatArray0 = new float[6][5];
      Room room0 = new Room(floatArray0);
      assertEquals(true, room0.isCeilingVisible());
      assertNotNull(room0);
      
      room0.setCeilingVisible(true);
      assertEquals(true, room0.isFloorVisible());
      assertEquals(true, room0.isAreaVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
  }

  @Test
  public void test67()  throws Throwable  {
      float[][] floatArray0 = new float[2][2];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      assertEquals(true, room0.isCeilingVisible());
      
      room0.setCeilingVisible(false);
      assertEquals(false, room0.isCeilingVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
  }

  @Test
  public void test68()  throws Throwable  {
      float[][] floatArray0 = new float[10][3];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.setCeilingShininess(0.0F);
      assertEquals(true, room0.isAreaVisible());
      assertEquals(true, room0.isFloorVisible());
      assertEquals(true, room0.isCeilingVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
  }

  @Test
  public void test69()  throws Throwable  {
      float[][] floatArray0 = new float[8][2];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.setCeilingShininess(1813.3564F);
      assertEquals(1813.3564F, room0.getCeilingShininess(), 0.01F);
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
  }

  @Test
  public void test70()  throws Throwable  {
      float[][] floatArray0 = new float[8][8];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.setLevel((Level) null);
      assertEquals(true, room0.isCeilingVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isAreaVisible());
      assertEquals(true, room0.isFloorVisible());
  }

  @Test
  public void test71()  throws Throwable  {
      float[][] floatArray0 = new float[2][7];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      Level level0 = new Level("eW).%\"~p?B", 0.0F, 1.0F, 0.0F);
      room0.setLevel(level0);
      assertEquals(true, room0.isAreaVisible());
      assertEquals(true, room0.isCeilingVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isFloorVisible());
  }

  @Test
  public void test72()  throws Throwable  {
      float[][] floatArray0 = new float[10][6];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      Level level0 = new Level("D!oH8u:$$%$u$sd^Hv", 0.0F, (-40.0F), (-897.6729F));
      boolean boolean0 = room0.isAtLevel(level0);
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isCeilingVisible());
      assertEquals(true, room0.isAreaVisible());
      assertEquals(false, boolean0);
      assertEquals(true, room0.isFloorVisible());
  }

  @Test
  public void test73()  throws Throwable  {
      float[][] floatArray0 = new float[3][3];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      boolean boolean0 = room0.isAtLevel((Level) null);
      assertEquals(true, room0.isFloorVisible());
      assertEquals(true, boolean0);
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isCeilingVisible());
      assertEquals(true, room0.isAreaVisible());
  }

  @Test
  public void test74()  throws Throwable  {
      float[][] floatArray0 = new float[8][8];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.getArea();
      room0.getArea();
      assertEquals(true, room0.isAreaVisible());
      assertEquals(true, room0.isCeilingVisible());
      assertEquals(true, room0.isFloorVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
  }

  @Test
  public void test75()  throws Throwable  {
      float[][] floatArray0 = new float[7][3];
      float[] floatArray1 = new float[9];
      floatArray1[1] = (-437.4447F);
      floatArray0[0] = floatArray1;
      float[] floatArray2 = new float[7];
      floatArray2[0] = (-645.01276F);
      floatArray0[1] = floatArray2;
      float[] floatArray3 = new float[10];
      floatArray3[0] = 1.0F;
      floatArray3[1] = (-849.45746F);
      floatArray0[2] = floatArray3;
      float[] floatArray4 = new float[4];
      floatArray4[0] = 1.0F;
      floatArray0[3] = floatArray4;
      floatArray0[6] = floatArray0[2];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.getArea();
      assertEquals(true, room0.isCeilingVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isFloorVisible());
      assertEquals(true, room0.isAreaVisible());
  }

  @Test
  public void test76()  throws Throwable  {
      float[][] floatArray0 = new float[7][3];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      boolean boolean0 = room0.isClockwise();
      assertEquals(0.0F, room0.getArea(), 0.01F);
      assertEquals(false, boolean0);
      assertEquals(true, room0.isAreaVisible());
      assertEquals(true, room0.isCeilingVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
  }

  @Test
  public void test77()  throws Throwable  {
      float[][] floatArray0 = new float[10][4];
      float[] floatArray1 = new float[6];
      floatArray1[0] = 1536.2104F;
      floatArray0[0] = floatArray1;
      floatArray0[1] = floatArray0[0];
      float[] floatArray2 = new float[10];
      floatArray2[1] = 1227.1195F;
      floatArray0[2] = floatArray2;
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      boolean boolean0 = room0.isClockwise();
      assertEquals(942556.9F, room0.getArea(), 0.01F);
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, boolean0);
      assertEquals(true, room0.isAreaVisible());
  }

  @Test
  public void test78()  throws Throwable  {
      float[][] floatArray0 = new float[3][1];
      float[] floatArray1 = new float[7];
      floatArray0[0] = floatArray1;
      floatArray0[1] = floatArray0[0];
      float[] floatArray2 = new float[1];
      floatArray2[0] = 86.49518F;
      floatArray0[2] = floatArray2;
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      int int0 = room0.getPointIndexAt(0.0F, (-353.64972F), 0.0F);
      assertEquals(true, room0.isFloorVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isAreaVisible());
      assertEquals(true, room0.isCeilingVisible());
      assertEquals((-1), int0);
  }

  @Test
  public void test79()  throws Throwable  {
      float[][] floatArray0 = new float[9][6];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      int int0 = room0.getPointIndexAt(0.0F, 0.0F, 0.0F);
      assertEquals(true, room0.isAreaVisible());
      assertEquals(true, room0.isCeilingVisible());
      assertEquals(0, int0);
      assertEquals(true, room0.isFloorVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
  }

  @Test
  public void test80()  throws Throwable  {
      float[][] floatArray0 = new float[8][8];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      boolean boolean0 = room0.isNameCenterPointAt((-998.8861F), 1186.5193F, (-1.0F));
      assertEquals(0.0F, room0.getNameXOffset(), 0.01F);
      assertEquals(true, room0.isCeilingVisible());
      assertEquals(true, room0.isFloorVisible());
      assertEquals(0.0F, room0.getXCenter(), 0.01F);
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isAreaVisible());
      assertEquals(false, boolean0);
  }

  @Test
  public void test81()  throws Throwable  {
      float[][] floatArray0 = new float[2][2];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      boolean boolean0 = room0.isNameCenterPointAt((-1.0F), 1813.3564F, 1813.3564F);
      assertEquals(true, room0.isCeilingVisible());
      assertEquals(0.0F, room0.getYCenter(), 0.01F);
      assertEquals(0.0F, room0.getNameXOffset(), 0.01F);
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(false, boolean0);
      assertEquals(0.0F, room0.getXCenter(), 0.01F);
  }

  @Test
  public void test82()  throws Throwable  {
      float[][] floatArray0 = new float[8][2];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      boolean boolean0 = room0.isAreaCenterPointAt(1813.3564F, 0.0F, 0.0F);
      assertEquals(true, room0.isFloorVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isCeilingVisible());
      assertEquals(0.0F, room0.getXCenter(), 0.01F);
      assertEquals(0.0F, room0.getAreaXOffset(), 0.01F);
      assertEquals(true, room0.isAreaVisible());
      assertEquals(false, boolean0);
  }

  @Test
  public void test83()  throws Throwable  {
      float[][] floatArray0 = new float[6][10];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      boolean boolean0 = room0.isAreaCenterPointAt((-1.0F), 770.6778F, 1.0F);
      assertEquals(false, boolean0);
      assertEquals(0.0F, room0.getXCenter(), 0.01F);
      assertEquals(0.0F, room0.getYCenter(), 0.01F);
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isAreaVisible());
      assertEquals(true, room0.isCeilingVisible());
  }

  @Test
  public void test84()  throws Throwable  {
      float[][] floatArray0 = new float[2][7];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.containsPoint((-1790.647F), 0.0F, (-1015.72906F));
      room0.containsPoint(411.8336F, 0.0F, (-156.09987F));
      assertEquals(true, room0.isCeilingVisible());
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isAreaVisible());
      assertEquals(true, room0.isFloorVisible());
  }

  @Test
  public void test85()  throws Throwable  {
      float[][] floatArray0 = new float[2][7];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.move((-427.01096F), (-1560.2045F));
      assertEquals((-1560.2045F), room0.getYCenter(), 0.01F);
      assertEquals((-427.01096F), room0.getXCenter(), 0.01F);
  }

  @Test
  public void test86()  throws Throwable  {
      float[][] floatArray0 = new float[9][6];
      Room room0 = new Room(floatArray0);
      assertNotNull(room0);
      
      room0.move(0.0F, 0.0F);
      assertEquals((-40.0F), room0.getNameYOffset(), 0.01F);
      assertEquals(true, room0.isAreaVisible());
      assertEquals(true, room0.isCeilingVisible());
      assertEquals(true, room0.isFloorVisible());
  }
}
