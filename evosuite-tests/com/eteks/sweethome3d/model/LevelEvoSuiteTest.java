/*
 * This file was automatically generated by EvoSuite
 */

package com.eteks.sweethome3d.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import com.eteks.sweethome3d.model.BackgroundImage;
import com.eteks.sweethome3d.model.Content;
import com.eteks.sweethome3d.model.Level;
import java.beans.PropertyChangeListener;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class LevelEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      Level level0 = new Level("", 398.96793F, 398.96793F, 398.96793F);
      float float0 = level0.getElevation();
      assertEquals(398.96793F, float0, 0.01F);
      assertEquals(398.96793F, level0.getFloorThickness(), 0.01F);
      assertEquals(true, level0.isVisible());
      assertEquals(398.96793F, level0.getHeight(), 0.01F);
  }

  @Test
  public void test1()  throws Throwable  {
      Level level0 = new Level("\"#g=Q", 840.33606F, 840.33606F, 0.0F);
      level0.getBackgroundImage();
      assertEquals(0.0F, level0.getHeight(), 0.01F);
      assertEquals(true, level0.isVisible());
      assertEquals(840.33606F, level0.getElevation(), 0.01F);
      assertEquals(840.33606F, level0.getFloorThickness(), 0.01F);
  }

  @Test
  public void test2()  throws Throwable  {
      Level level0 = new Level((String) null, (-1.0F), (-1.0F), (-1.0F));
      level0.removePropertyChangeListener((PropertyChangeListener) null);
      assertEquals((-1.0F), level0.getHeight(), 0.01F);
      assertEquals((-1.0F), level0.getElevation(), 0.01F);
      assertEquals(true, level0.isVisible());
      assertEquals((-1.0F), level0.getFloorThickness(), 0.01F);
  }

  @Test
  public void test3()  throws Throwable  {
      Level level0 = new Level((String) null, (-1.0F), (-1.0F), (-1.0F));
      level0.addPropertyChangeListener((PropertyChangeListener) null);
      assertEquals((-1.0F), level0.getElevation(), 0.01F);
      assertEquals(true, level0.isVisible());
      assertEquals((-1.0F), level0.getFloorThickness(), 0.01F);
      assertEquals((-1.0F), level0.getHeight(), 0.01F);
  }

  @Test
  public void test4()  throws Throwable  {
      Level level0 = new Level("x", 1617.0793F, (-1419.729F), (-1419.729F));
      Level level1 = level0.clone();
      assertEquals((-1419.729F), level0.getHeight(), 0.01F);
      assertNotSame(level0, level1);
      assertEquals(true, level0.isVisible());
      assertEquals((-1419.729F), level0.getFloorThickness(), 0.01F);
      assertEquals(1617.0793F, level0.getElevation(), 0.01F);
  }

  @Test
  public void test5()  throws Throwable  {
      Level level0 = new Level("A~)", (-1.9751844F), (-1.9751844F), (-1.9751844F));
      boolean boolean0 = level0.isVisible();
      assertEquals(true, boolean0);
      assertEquals((-1.9751844F), level0.getFloorThickness(), 0.01F);
      assertEquals((-1.9751844F), level0.getHeight(), 0.01F);
      assertEquals((-1.9751844F), level0.getElevation(), 0.01F);
  }

  @Test
  public void test6()  throws Throwable  {
      Level level0 = new Level("x", 1617.0793F, (-1419.729F), (-1419.729F));
      float float0 = level0.getFloorThickness();
      assertEquals((-1419.729F), float0, 0.01F);
      assertEquals(true, level0.isVisible());
      assertEquals((-1419.729F), level0.getHeight(), 0.01F);
      assertEquals(1617.0793F, level0.getElevation(), 0.01F);
  }

  @Test
  public void test7()  throws Throwable  {
      Level level0 = new Level("x", 1617.0793F, (-1419.729F), (-1419.729F));
      float float0 = level0.getHeight();
      assertEquals((-1419.729F), level0.getFloorThickness(), 0.01F);
      assertEquals(1617.0793F, level0.getElevation(), 0.01F);
      assertEquals(true, level0.isVisible());
      assertEquals((-1419.729F), float0, 0.01F);
  }

  @Test
  public void test8()  throws Throwable  {
      Level level0 = new Level("A~)", (-1.9751844F), (-1.9751844F), (-1.9751844F));
      String string0 = level0.getName();
      assertEquals((-1.9751844F), level0.getHeight(), 0.01F);
      assertEquals((-1.9751844F), level0.getFloorThickness(), 0.01F);
      assertNotNull(string0);
      assertEquals((-1.9751844F), level0.getElevation(), 0.01F);
      assertEquals(true, level0.isVisible());
  }

  @Test
  public void test9()  throws Throwable  {
      Level level0 = new Level((String) null, (-11.867401F), (-11.867401F), (-11.867401F));
      level0.setName("?z}");
      assertEquals((-11.867401F), level0.getHeight(), 0.01F);
      assertEquals((-11.867401F), level0.getFloorThickness(), 0.01F);
      assertEquals(true, level0.isVisible());
      assertEquals((-11.867401F), level0.getElevation(), 0.01F);
  }

  @Test
  public void test10()  throws Throwable  {
      Level level0 = new Level((String) null, (-1.0F), (-1.0F), (-1.0F));
      level0.setName((String) null);
      assertEquals((-1.0F), level0.getHeight(), 0.01F);
      assertEquals(true, level0.isVisible());
      assertEquals((-1.0F), level0.getFloorThickness(), 0.01F);
      assertEquals((-1.0F), level0.getElevation(), 0.01F);
  }

  @Test
  public void test11()  throws Throwable  {
      Level level0 = new Level("#j!i", (-889.8436F), (-889.8436F), (-889.8436F));
      assertEquals((-889.8436F), level0.getElevation(), 0.01F);
      
      level0.setElevation((-889.8436F));
      assertEquals((-889.8436F), level0.getHeight(), 0.01F);
      assertEquals(true, level0.isVisible());
      assertEquals((-889.8436F), level0.getFloorThickness(), 0.01F);
  }

  @Test
  public void test12()  throws Throwable  {
      Level level0 = new Level("x", 1617.0793F, (-1419.729F), (-1419.729F));
      assertEquals(1617.0793F, level0.getElevation(), 0.01F);
      
      level0.setElevation((-1419.729F));
      assertEquals((-1419.729F), level0.getElevation(), 0.01F);
      assertEquals((-1419.729F), level0.getHeight(), 0.01F);
  }

  @Test
  public void test13()  throws Throwable  {
      Level level0 = new Level("#j!i", (-889.8436F), (-889.8436F), (-889.8436F));
      assertEquals((-889.8436F), level0.getFloorThickness(), 0.01F);
      
      level0.setFloorThickness((-889.8436F));
      assertEquals((-889.8436F), level0.getHeight(), 0.01F);
      assertEquals(true, level0.isVisible());
      assertEquals((-889.8436F), level0.getElevation(), 0.01F);
  }

  @Test
  public void test14()  throws Throwable  {
      Level level0 = new Level("\"#g=Q", 840.33606F, 840.33606F, 0.0F);
      assertEquals(840.33606F, level0.getFloorThickness(), 0.01F);
      
      level0.setFloorThickness(0.0F);
      assertEquals(0.0F, level0.getFloorThickness(), 0.01F);
  }

  @Test
  public void test15()  throws Throwable  {
      Level level0 = new Level("A~)", (-1.9751844F), (-1.9751844F), (-1.9751844F));
      assertEquals((-1.9751844F), level0.getHeight(), 0.01F);
      
      level0.setHeight((-1.9751844F));
      assertEquals((-1.9751844F), level0.getElevation(), 0.01F);
      assertEquals(true, level0.isVisible());
      assertEquals((-1.9751844F), level0.getFloorThickness(), 0.01F);
  }

  @Test
  public void test16()  throws Throwable  {
      Level level0 = new Level("Ce\"*Z*'<d7%-^", 51.64561F, 51.64561F, 51.64561F);
      level0.setHeight(16.32495F);
      assertEquals(16.32495F, level0.getHeight(), 0.01F);
  }

  @Test
  public void test17()  throws Throwable  {
      Level level0 = new Level("\"#g=Q", 840.33606F, 840.33606F, 0.0F);
      level0.setBackgroundImage((BackgroundImage) null);
      assertEquals(true, level0.isVisible());
      assertEquals(840.33606F, level0.getElevation(), 0.01F);
      assertEquals(0.0F, level0.getHeight(), 0.01F);
      assertEquals(840.33606F, level0.getFloorThickness(), 0.01F);
  }

  @Test
  public void test18()  throws Throwable  {
      Level level0 = new Level("", 398.96793F, 398.96793F, 398.96793F);
      BackgroundImage backgroundImage0 = new BackgroundImage((Content) null, 398.96793F, 398.96793F, 398.96793F, 398.96793F, 398.96793F, 398.96793F, 398.96793F, false);
      level0.setBackgroundImage(backgroundImage0);
      assertEquals(398.96793F, level0.getHeight(), 0.01F);
      assertEquals(true, level0.isVisible());
      assertEquals(398.96793F, level0.getElevation(), 0.01F);
      assertEquals(398.96793F, level0.getFloorThickness(), 0.01F);
  }

  @Test
  public void test19()  throws Throwable  {
      Level level0 = new Level("Ce\"*Z*'<d7%-^", 51.64561F, 51.64561F, 51.64561F);
      assertEquals(true, level0.isVisible());
      
      level0.setVisible(true);
      assertEquals(51.64561F, level0.getFloorThickness(), 0.01F);
      assertEquals(51.64561F, level0.getElevation(), 0.01F);
      assertEquals(51.64561F, level0.getHeight(), 0.01F);
  }

  @Test
  public void test20()  throws Throwable  {
      Level level0 = new Level("\"#g=Q", 840.33606F, 840.33606F, 0.0F);
      level0.setVisible(false);
      assertEquals(false, level0.isVisible());
      
      level0.setVisible(true);
      assertEquals(0.0F, level0.getHeight(), 0.01F);
  }
}
