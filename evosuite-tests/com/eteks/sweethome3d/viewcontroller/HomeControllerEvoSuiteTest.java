/*
 * This file was automatically generated by EvoSuite
 */

package com.eteks.sweethome3d.viewcontroller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import com.eteks.sweethome3d.model.Compass;
import com.eteks.sweethome3d.model.Home;
import com.eteks.sweethome3d.model.Selectable;
import com.eteks.sweethome3d.model.UserPreferences;
import com.eteks.sweethome3d.swing.SwingViewFactory;
import com.eteks.sweethome3d.viewcontroller.ContentManager;
import com.eteks.sweethome3d.viewcontroller.FurnitureController;
import com.eteks.sweethome3d.viewcontroller.HomeController;
import com.eteks.sweethome3d.viewcontroller.View;
import com.eteks.sweethome3d.viewcontroller.ViewFactory;
import java.util.List;
import java.util.Vector;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class HomeControllerEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      Home home0 = new Home();
      HomeController homeController0 = new HomeController(home0, (UserPreferences) null, (ViewFactory) null);
      // Undeclared exception!
      try {
        homeController0.close();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test1()  throws Throwable  {
      Home home0 = new Home((-1852.4812F));
      SwingViewFactory swingViewFactory0 = new SwingViewFactory();
      HomeController homeController0 = new HomeController(home0, (UserPreferences) null, (ViewFactory) swingViewFactory0, (ContentManager) null);
      assertEquals("", homeController0.getVersion());
  }

  @Test
  public void test2()  throws Throwable  {
      Home home0 = new Home();
      SwingViewFactory swingViewFactory0 = new SwingViewFactory();
      HomeController homeController0 = new HomeController(home0, (UserPreferences) null, (ViewFactory) swingViewFactory0);
      // Undeclared exception!
      try {
        homeController0.modifyBackgroundImage();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test3()  throws Throwable  {
      Home home0 = new Home();
      SwingViewFactory swingViewFactory0 = new SwingViewFactory();
      HomeController homeController0 = new HomeController(home0, (UserPreferences) null, (ViewFactory) swingViewFactory0);
      // Undeclared exception!
      try {
        homeController0.saveAndCompress();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test4()  throws Throwable  {
      Home home0 = new Home();
      SwingViewFactory swingViewFactory0 = new SwingViewFactory();
      HomeController homeController0 = new HomeController(home0, (UserPreferences) null, (ViewFactory) swingViewFactory0);
      // Undeclared exception!
      try {
        homeController0.hideBackgroundImage();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test5()  throws Throwable  {
      Home home0 = new Home();
      SwingViewFactory swingViewFactory0 = new SwingViewFactory();
      HomeController homeController0 = new HomeController(home0, (UserPreferences) null, (ViewFactory) swingViewFactory0);
      // Undeclared exception!
      try {
        homeController0.showBackgroundImage();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test6()  throws Throwable  {
      Home home0 = new Home();
      SwingViewFactory swingViewFactory0 = new SwingViewFactory();
      HomeController homeController0 = new HomeController(home0, (UserPreferences) null, (ViewFactory) swingViewFactory0);
      // Undeclared exception!
      try {
        homeController0.editPreferences();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test7()  throws Throwable  {
      Home home0 = new Home();
      SwingViewFactory swingViewFactory0 = new SwingViewFactory();
      HomeController homeController0 = new HomeController(home0, (UserPreferences) null, (ViewFactory) swingViewFactory0);
      ContentManager contentManager0 = homeController0.getContentManager();
      assertNull(contentManager0);
  }

  @Test
  public void test8()  throws Throwable  {
      Home home0 = new Home();
      HomeController homeController0 = new HomeController(home0, (UserPreferences) null, (ViewFactory) null);
      // Undeclared exception!
      try {
        homeController0.zoomIn();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test9()  throws Throwable  {
      Home home0 = new Home();
      SwingViewFactory swingViewFactory0 = new SwingViewFactory();
      HomeController homeController0 = new HomeController(home0, (UserPreferences) null, (ViewFactory) swingViewFactory0);
      // Undeclared exception!
      try {
        homeController0.previewPrint();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test10()  throws Throwable  {
      Home home0 = new Home();
      HomeController homeController0 = new HomeController(home0, (UserPreferences) null, (ViewFactory) null);
      // Undeclared exception!
      try {
        homeController0.focusedViewChanged((View) null);
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test11()  throws Throwable  {
      Home home0 = new Home();
      SwingViewFactory swingViewFactory0 = new SwingViewFactory();
      HomeController homeController0 = new HomeController(home0, (UserPreferences) null, (ViewFactory) swingViewFactory0);
      homeController0.setVisualProperty("/F QAv<'", "");
      assertEquals("", homeController0.getVersion());
  }

  @Test
  public void test12()  throws Throwable  {
      Home home0 = new Home();
      HomeController homeController0 = new HomeController(home0, (UserPreferences) null, (ViewFactory) null);
      // Undeclared exception!
      try {
        homeController0.createPhoto();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test13()  throws Throwable  {
      Home home0 = new Home();
      SwingViewFactory swingViewFactory0 = new SwingViewFactory();
      HomeController homeController0 = new HomeController(home0, (UserPreferences) null, (ViewFactory) swingViewFactory0);
      // Undeclared exception!
      try {
        homeController0.deleteRecentHomes();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test14()  throws Throwable  {
      Home home0 = new Home((-16.568972F));
      SwingViewFactory swingViewFactory0 = new SwingViewFactory();
      HomeController homeController0 = new HomeController(home0, (UserPreferences) null, (ViewFactory) swingViewFactory0);
      homeController0.deleteBackgroundImage();
      assertEquals("", homeController0.getVersion());
  }

  @Test
  public void test15()  throws Throwable  {
      Home home0 = new Home(10.328615F);
      SwingViewFactory swingViewFactory0 = new SwingViewFactory();
      HomeController homeController0 = new HomeController(home0, (UserPreferences) null, (ViewFactory) swingViewFactory0);
      // Undeclared exception!
      try {
        homeController0.modifySelectedFurniture();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test16()  throws Throwable  {
      Home home0 = new Home();
      SwingViewFactory swingViewFactory0 = new SwingViewFactory();
      HomeController homeController0 = new HomeController(home0, (UserPreferences) null, (ViewFactory) swingViewFactory0);
      FurnitureController furnitureController0 = homeController0.getFurnitureController();
      assertNotNull(furnitureController0);
  }

  @Test
  public void test17()  throws Throwable  {
      Home home0 = new Home(1887.0593F);
      SwingViewFactory swingViewFactory0 = new SwingViewFactory();
      HomeController homeController0 = new HomeController(home0, (UserPreferences) null, (ViewFactory) swingViewFactory0);
      Vector<Compass> vector0 = new Vector<Compass>();
      homeController0.paste((List<? extends Selectable>) vector0);
      assertEquals(10, vector0.capacity());
  }

  @Test
  public void test18()  throws Throwable  {
      Home home0 = new Home();
      SwingViewFactory swingViewFactory0 = new SwingViewFactory();
      HomeController homeController0 = new HomeController(home0, (UserPreferences) null, (ViewFactory) swingViewFactory0);
      // Undeclared exception!
      try {
        homeController0.newHome();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test19()  throws Throwable  {
      Home home0 = new Home();
      SwingViewFactory swingViewFactory0 = new SwingViewFactory();
      HomeController homeController0 = new HomeController(home0, (UserPreferences) null, (ViewFactory) swingViewFactory0);
      List<String> list0 = homeController0.getRecentHomes();
      assertEquals(0, list0.size());
  }

  @Test
  public void test20()  throws Throwable  {
      Home home0 = new Home();
      SwingViewFactory swingViewFactory0 = new SwingViewFactory();
      HomeController homeController0 = new HomeController(home0, (UserPreferences) null, (ViewFactory) swingViewFactory0);
      String string0 = homeController0.getVersion();
      assertEquals("", string0);
  }

  @Test
  public void test21()  throws Throwable  {
      Home home0 = new Home();
      SwingViewFactory swingViewFactory0 = new SwingViewFactory();
      HomeController homeController0 = new HomeController(home0, (UserPreferences) null, (ViewFactory) swingViewFactory0);
      homeController0.detachView((View) null);
      assertEquals("", homeController0.getVersion());
  }

  @Test
  public void test22()  throws Throwable  {
      Home home0 = new Home(1.0F);
      SwingViewFactory swingViewFactory0 = new SwingViewFactory();
      HomeController homeController0 = new HomeController(home0, (UserPreferences) null, (ViewFactory) swingViewFactory0);
      // Undeclared exception!
      try {
        homeController0.help();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }
}
