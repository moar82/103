/*
 * This file was automatically generated by EvoSuite
 */

package com.eteks.sweethome3d.io;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import com.eteks.sweethome3d.io.DefaultTexturesCatalog;
import com.eteks.sweethome3d.io.DefaultUserPreferences;
import com.eteks.sweethome3d.model.UserPreferences;
import java.io.File;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import org.evosuite.Properties.SandboxMode;
import org.evosuite.sandbox.Sandbox;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class DefaultTexturesCatalogEvoSuiteTest {

  private static ExecutorService executor; 

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
    org.evosuite.Properties.SANDBOX_MODE = SandboxMode.RECOMMENDED; 
    Sandbox.initializeSecurityManagerForSUT(); 
    executor = Executors.newCachedThreadPool(); 
  } 

  @AfterClass 
  public static void clearEvoSuiteFramework(){ 
    executor.shutdownNow(); 
    Sandbox.resetDefaultSecurityManager(); 
  } 

  @Before 
  public void initTestCase(){ 
    Sandbox.goingToExecuteSUTCode(); 
  } 

  @After 
  public void doneWithTestCase(){ 
    Sandbox.doneWithExecutingSUTCode(); 
  } 


  @Test
  public void test0()  throws Throwable  {
    Future<?> future = executor.submit(new Runnable(){ 
            public void run() { 
          File file0 = new File("");
          File file1 = file0.getAbsoluteFile();
          DefaultTexturesCatalog defaultTexturesCatalog0 = null;
          try {
            defaultTexturesCatalog0 = new DefaultTexturesCatalog(file1);
            fail("Expecting exception: SecurityException");
          } catch(SecurityException e) {
            /*
             * Security manager blocks (java.io.FilePermission /mnt/fastdata/ac1gf/SF110/dist/103_sweethome3d/sweethome3d.jar write)
             * java.lang.Thread.getStackTrace(Thread.java:1479)
             * org.evosuite.sandbox.MSecurityManager.checkPermission(MSecurityManager.java:303)
             * java.lang.SecurityManager.checkWrite(SecurityManager.java:962)
             * java.io.File.canWrite(File.java:711)
             * com.eteks.sweethome3d.io.DefaultTexturesCatalog.readPluginTexturesCatalog(DefaultTexturesCatalog.java:220)
             * com.eteks.sweethome3d.io.DefaultTexturesCatalog.<init>(DefaultTexturesCatalog.java:164)
             * com.eteks.sweethome3d.io.DefaultTexturesCatalog.<init>(DefaultTexturesCatalog.java:136)
             * com.eteks.sweethome3d.io.DefaultTexturesCatalog.<init>(DefaultTexturesCatalog.java:127)
             * sun.reflect.GeneratedConstructorAccessor52.newInstance(Unknown Source)
             * sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:27)
             * java.lang.reflect.Constructor.newInstance(Constructor.java:513)
             * org.evosuite.testcase.ConstructorStatement$1.execute(ConstructorStatement.java:226)
             * org.evosuite.testcase.AbstractStatement.exceptionHandler(AbstractStatement.java:142)
             * org.evosuite.testcase.ConstructorStatement.execute(ConstructorStatement.java:188)
             * org.evosuite.testcase.TestRunnable.call(TestRunnable.java:291)
             * org.evosuite.testcase.TestRunnable.call(TestRunnable.java:44)
             * java.util.concurrent.FutureTask$Sync.innerRun(FutureTask.java:303)
             * java.util.concurrent.FutureTask.run(FutureTask.java:138)
             * java.util.concurrent.ThreadPoolExecutor$Worker.runTask(ThreadPoolExecutor.java:886)
             * java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:908)
             * java.lang.Thread.run(Thread.java:662)
             */
          }
      } 
    }); 
    future.get(6000, TimeUnit.MILLISECONDS); 
  }

  @Test
  public void test1()  throws Throwable  {
      DefaultUserPreferences defaultUserPreferences0 = new DefaultUserPreferences(false, (UserPreferences) null);
      File file0 = new File("s@eW[GYTEc?", "s@eW[GYTEc?");
      DefaultTexturesCatalog defaultTexturesCatalog0 = new DefaultTexturesCatalog((UserPreferences) defaultUserPreferences0, file0);
      assertEquals(3, defaultTexturesCatalog0.getCategoriesCount());
      assertNotNull(defaultTexturesCatalog0);
  }

  @Test
  public void test2()  throws Throwable  {
      URL[] uRLArray0 = new URL[15];
      DefaultTexturesCatalog defaultTexturesCatalog0 = new DefaultTexturesCatalog(uRLArray0);
      assertEquals(0, defaultTexturesCatalog0.getCategoriesCount());
  }

  @Test
  public void test3()  throws Throwable  {
      DefaultTexturesCatalog defaultTexturesCatalog0 = new DefaultTexturesCatalog();
      assertEquals(3, defaultTexturesCatalog0.getCategoriesCount());
      assertNotNull(defaultTexturesCatalog0);
  }
}
