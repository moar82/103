/*
 * This file was automatically generated by EvoSuite
 */

package com.eteks.sweethome3d.swing;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import com.eteks.sweethome3d.swing.AutoCommitSpinner;
import java.text.ParseException;
import javax.swing.JSpinner;
import javax.swing.SpinnerListModel;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class AutoCommitSpinnerEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      JSpinner jSpinner0 = new JSpinner();
      SpinnerNumberModel spinnerNumberModel0 = (SpinnerNumberModel)jSpinner0.getModel();
      AutoCommitSpinner autoCommitSpinner0 = new AutoCommitSpinner((SpinnerModel) spinnerNumberModel0);
      autoCommitSpinner0.commitEdit();
      assertEquals(0, autoCommitSpinner0.getHeight());
  }

  @Test
  public void test1()  throws Throwable  {
      Object[] objectArray0 = new Object[2];
      SpinnerListModel spinnerListModel0 = new SpinnerListModel(objectArray0);
      AutoCommitSpinner autoCommitSpinner0 = new AutoCommitSpinner((SpinnerModel) spinnerListModel0);
      assertEquals(false, autoCommitSpinner0.isShowing());
  }
}
