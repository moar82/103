/*
 * This file was automatically generated by EvoSuite
 */

package com.eteks.sweethome3d.swing;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import com.eteks.sweethome3d.model.UserPreferences;
import com.eteks.sweethome3d.swing.FileContentManager;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class FileContentManagerEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      FileContentManager fileContentManager0 = null;
      try {
        fileContentManager0 = new FileContentManager((UserPreferences) null);
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }
}
