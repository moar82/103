/*
 * This file was automatically generated by EvoSuite
 */

package com.eteks.sweethome3d;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import com.eteks.sweethome3d.HomeFrameController;
import com.eteks.sweethome3d.model.Home;
import com.eteks.sweethome3d.model.HomeApplication;
import com.eteks.sweethome3d.plugin.PluginManager;
import com.eteks.sweethome3d.viewcontroller.ContentManager;
import com.eteks.sweethome3d.viewcontroller.ViewFactory;
import java.io.File;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class HomeFrameControllerEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      Home home0 = new Home((-1074.5913F));
      File file0 = new File("EtZc/UTC", "EtZc/UTC");
      PluginManager pluginManager0 = new PluginManager(file0);
      HomeFrameController homeFrameController0 = new HomeFrameController(home0, (HomeApplication) null, (ViewFactory) null, (ContentManager) null, pluginManager0);
      // Undeclared exception!
      try {
        homeFrameController0.displayView();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }
}
